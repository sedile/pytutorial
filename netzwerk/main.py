import sys
from server import Server
from client import Client

if __name__ == "__main__":
    if (len(sys.argv) == 5):
        if sys.argv[4] == '-c':
            client = Client(sys.argv[1], sys.argv[2], sys.argv[3])
        else:
            print("Eingabefehler")
    elif (len(sys.argv) == 4):
        if sys.argv[3] == '-s':
            server = Server(sys.argv[1], sys.argv[2])
            server.run()
