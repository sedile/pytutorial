import sys
import socket
import threading

from client import Client

class Server:

    def __init__(self, host, port):
        self._connections = []        
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.bind((str(host), int(port)))
        self._sock.listen(1)

    def handler(self, client, address):
        while True:
            data = client.recv(1024)
            for connection in self._connections:
                connection.send(data)
            if not data:
                print("[!]",str(address[0]) + ":" + str(address[1]),"verbindung getrennt")
                self._connections.remove(client)
                client.close()
                break

    def run(self):
        while True:
            client, address = self._sock.accept()
            cThread = threading.Thread(target=self.handler, args=(client,address))
            cThread.daemon = True
            cThread.start()
            self._connections.append(client)
            print("[!]",str(address[0]) + ":" + str(address[1]),"verbunden")
