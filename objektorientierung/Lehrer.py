from Person import Person

class Lehrer(Person):

    def __init__(self,name,alter,geschlecht):
        Person.__init__(self,name,alter,geschlecht)

    def setKlasse(self,klasse):
        self._klasse = klasse

    def getKlasse(self):
        return self._klasse

    def setPersonID(self,pid):
        self._pid = pid

    def getPersonID(self):
        return self._pid
