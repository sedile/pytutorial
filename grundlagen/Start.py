# Noetige Imports von anderen Dateien
from grundlagen.Punkt import Punkt

# Die main-Funktion
if __name__ == "__main__":
    p = Punkt(3,5)
    q = Punkt.initFromPoint(p)
    print(p.getX()," ",p.getY())
    print(Punkt.anzahlPunkte)