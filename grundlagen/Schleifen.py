#Es koennen auch Dateien ohne Klassen definiert werden

def whileSchleife():
    x = 0
    while x <= 25:
        print(x)
        x = x + 1
        if x == 20:
            break

def forSchleife():
    x = ['Eins','Zwei','Drei']
        
    # for variable in sequenz
    for i in x:
        print(i, end=" -> ")
    print(' ')
    
    x = 16
    # range, um ein Iterationszaehler zu simulieren
    for i in range(x):
        print(i)

def forSchleifeRange(n):
    for i in range(0,n,2):
        if i % 2 == 0:
            print("Gerade")
        else:
            print("Ungerade")