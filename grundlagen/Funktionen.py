# Eine Klassendefinition

# Eine Globale Variable
x = 'global x'

class Funktionen:

    # Eine Funktionendefinition
    # self = Damit diese Funktion von einer Instanz aufgerufen werden kann
    # Ein Doppelpunkt am Ende ist Pflicht
    def rekursion(self, n):
        if n == 0:
            print("Rekursion ende")
        else:
            print("Eine weitere Rekursion")
            self.rekursion(n-1)

    # Ein kleines Beispiel, wie eine Fehlerbehandlung funktionieren koennte
    def eingabe(self):
        alter = input("Eine Zahl eingeben\n")
        try:
            if int(alter) < 0 or int(alter) > 100 :
                print('Bitte nur Zahlen zwischen 0 und 99 eingeben')
                self.eingabe()
                return
        except Exception as fehlertext:
            print("Bitte nur eine Zahl eingeben\n",fehlertext)
            self.eingabe()
        else:
            print("Eingegebene Zahl\n" + str(int(alter)) + ".")
        finally:
            pass

    # Einige Stringfunktionen
    def stringFunctions(self,txt):
        msg = "Eine Textausgabe"
        print(msg[5:]) # Gebe ab Zeichen 6 alles aus
        print(msg[5])
        print(msg[0:10:2]) # Von Zeichen 0 bis 10 nur alle 2 'Schritte'
        print(msg.upper())
        print(msg.count('e'))
        print(msg.find('aus')) # Gebe Startindex zurueck
        msg = msg.replace('e','X')
        total = msg + ' ' + txt
        print(total)
        total = '{}, {} Welcome'.format('Platz 1','Platz 2')
        print(total)

    # Casten
    def rechnen(self):
        num_1 = '100'
        num_2 = 3
        res = int(num_1) + num_2 # Casten von ein Datentyp in ein anderen
        print(res)
        
        num_1 = 5
        num_2 = 3
        res = num_1 / num_2
        print(res)
        res = num_1 // num_2 # Division wird abgerundet auf die naechste ganze Zahl
        print(res)
        
        res = num_1 ** num_2 # Potenzen
        print(res)

    def dateiLesen(self):
        f = open('Start.py','r')
        for zeile in f:
            print(zeile)

    def dateiSchreiben(self):
        f = open('test','w')
        
        x = 5
        while x > 0:
            f.write('x')
            x = x - 1

    # Bei mehreren Bedingungen elif, bei nur zwei if-else
    def bedingungen(self):
        num = 5
        wahr = False
        if num < 5:
            print("kleiner als 5")
        elif num >= 5 and wahr:
            print("Groessergleich 5 und wahr")
        elif num >= 5 and not wahr:
            print("Groessergleich 5 und falsch")

    # Global und Lokal
    def scope(self):
        global x
        print(x)
        x = 'lokal x' # Globale Variable wird ueberschrieben
        print(x)
