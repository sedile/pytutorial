# Ein Beispiel einer einfachen Klasse
# Variablen ohne self sind Klassenvariablen (static)
# Konvention : _<Attr> oder _<Fkt> soll zeigen, dass es sich um 'private' handelt

class Punkt:

    anzahlPunkte = 0 # Klassenvariable (Punkt.anzahlPunkte)

    # Der Konstruktor
    # self-Variablen sind Instanzvariablen
    def __init__(self, x, y):
        self._x = x
        self._y = y
        Punkt.anzahlPunkte += 1

    # Klassenmethode (Fabrikmethode), um ueberladene Konstruktoren zu simulieren
    @classmethod
    def initOrigin(cls):
        return cls(0,0)

    @classmethod
    def initFromPoint(cls, punkt):
        return cls(punkt.getX(),punkt.getY())

    # Setterfunktionen
    def setX(self, x):
        self._x = x

    def setY(self, y):
        self._y = y

    # Getterfunktionen
    def getX(self):
        return self._x

    def getY(self):
        return self._y
