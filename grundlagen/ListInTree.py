class ListInTree:
    
    def __init__(self):
        self._sequence = (7)
        #self._sequence = (11,5,47)
        self._tree = (7,-3,11,5,11,-1,17,3,-1,7,5,0,1,1,0,17,15,-1,70,43,35,47,3,1,5,-34,1,89,5,6,0)
        self._solution = []
        
    def findElem(self):
        l_idx = 0
        idx = 0

        while idx < len(self._tree):
            if ( self._sequence[l_idx] == self._tree[idx] ):
                (l_idx, idx) = self._findSequenceInTree(l_idx + 1, idx)
                if ( l_idx == - 1):
                    return
            else:
                idx += 1
                
    def _findSequenceInTree(self, l_idx, t_idx):
        self._solution.append(t_idx)
        
        if ( len(self._tree) < 2*t_idx + 2 or l_idx == len(self._sequence)):
            return (-1,-1)
        
        if ( self._sequence[l_idx] == self._tree[2*t_idx+1] ):
            return (l_idx, 2*t_idx+1)
        elif ( self._sequence[l_idx] == self._tree[2*t_idx+2] ):
            return (l_idx, 2*t_idx+2)
        else:
            self._solution.clear()
            return (0, t_idx + 1)
        
    def getSolution(self):
        return self._solution
        
x = ListInTree()
x.findElem()
print(x.getSolution())