class LambdaUtil:

    def consumer(self, x, expr):
        expr(x)

    def biConsumer(self, x, y, expr):
        expr(x,y)

    def triConsumer(self, x, y, z, expr):
        expr(x,y,z)

    def function(self, x, expr):
        return expr(x)

    def biFunction(self, x, y, expr):
        return expr(x,y)

    def triFunction(self, x, y, z, expr):
        return expr(x,y,z)

# Beispiel
#obj = LambdaUtil()
#l = lambda x,y : x + y
#res = obj.biFunction(2,3,l)
#print(res)