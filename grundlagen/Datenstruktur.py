class Datenstruktur:

    def liste(self):

        # Listendefinition
        liste = [1,2,3,4,5]

        # Teilbereich drucken
        print(liste[2:])

        # Auf Elemente 0 bis 2 zugreifen
        print(liste[0:3])

        # Elementzugriff
        element = liste[2]
        print(element)

        # Liste manipulieren
        liste.append(666) # Element hinzufuegen
        liste.insert(0,35)
        liste.remove(3)
        liste.pop()

        # Durchlauf
        for item in liste:
            print(item)
            
        liste2 = [36,37]
        liste.extend(liste2) # Elemente der Datenstruktur hinzufuegen
        print(liste)

        # Liste/Tupel/Set/Dict sortieren 
        sort = sorted(liste)
        print(sort)
        
    def subliste(self):
        # Jede Datenstruktur kann Unterstrukturen aufnehmen (Beispiel : Liste)
        liste = [1,2,(True,False),3,['1',{'a':1,'b':2},'3'],4,[True,False]]
        
        elem = liste[4]
        subelem = elem[1]
        
        for key in subelem:
            print(key)  

    # Tupel koennen nicht manipuliert werden (immutable)
    def tupel(self):

        # Tupeldefinition
        tupel = ('a','b','c','d','e')
        
        contains = 'x' not in tupel
        print(contains)
        
        # Index eines Wertes ermitteln
        idx = tupel.index('d')
        print(idx)

        # Tupelzugriff (read)
        element = tupel[2]
        print(element)

    # Set ist wie eine mathematische Menge
    def menge(self):

        # Setdefinition
        mengeA = {(1, 'NRW'),(2, 'Bayern'),(3, 'RP'),(4, 'Niedersachsen')}
        mengeB = {(5, 'Berlin'),(3, 'RP')}
        
        # Menge kann veraendert werden, nicht deren Inhalte
        mengeB.add((6, 'Sachsen'))
        mengeB.remove((5, 'Berlin'))

        # Pruefen, ob Element in der Menge ist
        print((1,'NRW') in mengeA)
        
        # Unterschiede
        print(mengeA.difference(mengeB))
        
        # Vereinigung
        mengeC = mengeA.union(mengeB)
        print(mengeC)

    # Dictionary sind wie eine Map
    def dictionary(self):
   
        # Mapdefinition (Key : Value)
        bitflag = {0 : "Nein" , 1 : "Mal sehen", 2 : "Vielleicht", 3 : "Ja"}

        # Key entnehmen, zweiter Param falls Key nicht gibt
        element = bitflag.pop(2,'unbekannt')
        print(element)

        # Paar hinzufuegen
        paar = {5 : "Zusatz"}
        bitflag.update(paar)

        # Iteration (Key)
        for key in bitflag:
            print(key)

        # Iteration (Value)
        for value in bitflag.values():
            print(value)

        # Iteration (Paar)
        for key, value in bitflag.items():
            print(key, value)
